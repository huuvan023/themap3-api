package com.dgm.map3.utils;

import com.tdx.http.server.BaseResultType;
import com.tdx.http.server.ResultTypes;

public class ResultTypesV2 extends ResultTypes {
    public static BaseResultType FILE_TOO_LARGE = new BaseResultType(413, 1, "FILE TOO LARGE!");
    public static BaseResultType FILE_WRONG_FORMAT = new BaseResultType(422, 1, "FILE WRONG FORMAT! ONLY ACCEPT PNG AND JPG");

}
