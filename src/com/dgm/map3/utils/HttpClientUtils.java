package com.dgm.map3.utils;

import com.ligerdev.appbase.utils.BaseUtils;
import com.ligerdev.appbase.utils.http.*;
import com.ligerdev.appbase.utils.http.HttpServerUtils;
import com.ligerdev.appbase.utils.http.PostResult;
import com.ligerdev.appbase.utils.textbase.Log4jLoader;
import com.tdx.http.client.DeleteResult;
import com.tdx.http.client.PutResult;
import com.tdx.text.ConsoleK;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import sun.misc.BASE64Encoder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class HttpClientUtils {

    private HttpClient httpClient = null;
    private Logger logger = Log4jLoader.getLogger();
    private static ConcurrentHashMap<String, HttpClientUtils>
            listInstance = new ConcurrentHashMap<String, HttpClientUtils>();

    private HttpClientUtils(int timeout) {
        try {
            MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
            HttpConnectionManagerParams params = new HttpConnectionManagerParams();
            params.setDefaultMaxConnectionsPerHost(800);
            params.setMaxTotalConnections(1500);

            params.setParameter(HttpConnectionManagerParams.SO_TIMEOUT, timeout);
            params.setParameter(HttpConnectionManagerParams.CONNECTION_TIMEOUT, timeout);
            connectionManager.setParams(params);

            httpClient = new HttpClient(connectionManager);
            httpClient.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));

        } catch (Throwable e) {
            logger.info("Exception: " + e.getMessage());
        }
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public static void disableDebugLog() {

        Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.WARN);
        Logger.getLogger("org.apache.http.wire").setLevel(Level.WARN);
        Logger.getLogger("httpclient.wire.header").setLevel(Level.WARN);
        Logger.getLogger("httpclient.wire.content").setLevel(Level.WARN);
		
    }

    public synchronized static HttpClientUtils getInstance(int timeout) {
        HttpClientUtils tmp = listInstance.get(timeout + "");
        if (tmp == null) {
            tmp = new HttpClientUtils(timeout);
            listInstance.put(timeout + "", tmp);
        }
        return tmp;
    }

    public static String getLocationRedirect(String transid, Logger logger,
                                             String urlToRead, int timeout, Hashtable<String, String> headers) {

        HttpURLConnection httpConnection = null;
        try {
            URL url = new URL(urlToRead);
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setInstanceFollowRedirects(false);
            httpConnection.setRequestMethod("GET");

            if (headers != null) {
                Enumeration<String> enums = headers.keys();
                while (enums.hasMoreElements()) {
                    String key = enums.nextElement();
                    String value = headers.get(key);
                    httpConnection.addRequestProperty(key, value);
                }
            }
            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);
            httpConnection.connect();
            int responseCode = httpConnection.getResponseCode();
            String location = httpConnection.getHeaderField("Location");
            return location;

        } catch (Exception e) {
            logger.info(transid + ", Exception: " + e.getMessage() + "/ url: " + urlToRead, e);
        } finally {
            try {
                httpConnection.disconnect();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public String get(String transid, String url) {
        return getRs(transid, url).getBody();
    }

    public GetResult getRs(String transid, String url) {
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
        method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2");
        try {
            httpClient.executeMethod(method);
            if (method.getStatusCode() == HttpStatus.SC_OK) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                GetResult httpResult = new GetResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", GET Redirect, url = " + url + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(GET) fail, url = " + url + "; respCode = " + method.getStatusCode());
            }
        } catch (Exception e) {
            logger.info(transid + ", url = " + url + ", Exception: " + e.getMessage(), e);
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        GetResult httpResult = new GetResult(method, null);
        return httpResult;
    }

    public String get(String transid, String url, String user, String pass) {
        return getRs(transid, url, user, pass).getBody();
    }

    public GetResult getRs(String transid, String url, String user, String pass) {
        GetMethod method = new GetMethod(url);
        method.setRequestHeader("Content-Type", "text/xml; charset=UTF-8");
        method.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2");
        try {
            BASE64Encoder encoder = new BASE64Encoder();
            String encoding = encoder.encode((user + ":" + pass).getBytes());
            method.setRequestHeader("Authorization", "Basic " + encoding);

            httpClient.executeMethod(method);
            if (method.getStatusCode() == HttpStatus.SC_OK) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                GetResult httpResult = new GetResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", GET Redirect, url = " + url + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(GET) fail, url = " + url + "; respCode = " + method.getStatusCode());
            }
        } catch (Exception e) {
            logger.info(transid + ", url = " + url + ", Exception: " + e.getMessage(), e);
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        GetResult httpResult = new GetResult(method, null);
        return httpResult;
    }

    public String get(String transid, String url, ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers, boolean enableLog) {
        long start = System.currentTimeMillis();
        GetResult getResult = getRs(transid, url, params, headers);
        String responseBody = getResult.getBody();
        if (enableLog) {
            logInfoRequest(transid, url, "GET", null, start, headers, params, responseBody);
        }
        return responseBody;
    }

    public GetResult getRs(String transid, String url, ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        GetMethod method = new GetMethod();
        try {
            url = url.trim();
            if (!url.contains("?")) {
                url += "?";
            }
            for (int i = 0; params != null && i < params.size(); i++) {
                NameValuePair nvp = params.get(i);
                url += "&" + URLEncoder.encode(nvp.getName(), "UTF-8")
                        + "=" + URLEncoder.encode(nvp.getValue(), "UTF-8");
            }
            for (int i = 0; headers != null && i < headers.size(); i++) {
                NameValuePair nvp = headers.get(i);
                method.addRequestHeader(nvp.getName(), nvp.getValue());
            }
            method.setURI(new URI(url));
            httpClient.executeMethod(method);
            if (method.getStatusCode() >= HttpStatus.SC_OK && method.getStatusCode() < 300) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                GetResult httpResult = new GetResult(method, response);
                return httpResult;
            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", GET Redirect, url = " + url + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(GET) fail, url = " + url + "; respCode = " + method.getStatusCode());
            }
        } catch (Exception e) {
            logger.info(transid + ", url = " + url + ", Exception: " + e.getMessage(), e);
        } finally {
            if (method != null) {
                method.releaseConnection();
            }
        }
        GetResult httpResult = new GetResult(method, null);
        return httpResult;
    }

    public String post(String transid, String url, String txt) {
        return postRs(transid, url, txt).getBody();
    }

    public PostResult postRs(String transid, String url, String txt) {
        PostMethod method = new PostMethod(url);
        try {
            ByteArrayRequestEntity entity = new ByteArrayRequestEntity(txt.getBytes("UTF-8"));
            method.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
            method.setRequestEntity(entity);
            httpClient.executeMethod(method);

            if (method.getStatusCode() >= 200 && method.getStatusCode() < 300) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                PostResult httpResult = new PostResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", POST Redirect, url = " + url + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(POST) fail, url = " + url + "; request = " + txt + "; respCode = " + method.getStatusCode());
            }
        } catch (Throwable e) {
            logger.info(transid + ", url = " + url + ", Exception: " + e.getMessage(), e);
        } finally {
            method.releaseConnection();
        }
        PostResult httpResult = new PostResult(method, null);
        return httpResult;
    }

    public String post(String transid, String url, String txt,
                       ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers, boolean enableLog) {
        long start = System.currentTimeMillis();
        PostResult postResult = postRs(transid, url, txt, params, headers, 3, 0);
        String body = postResult.getBody();
        if (enableLog) {
            logInfoRequest(transid, url, "POST", txt, start, headers, params, body);
        }
        return body;
    }

    public String post(String transid, String url, String txt,
                       ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        return post(transid, url, txt, params, headers, true);
    }

    protected void logInfoRequest(String transid, String url, String method, String requestBody, long start, Object headers, Object params, String responseBody) {
        String log = String.format("===Transid [%s]: call [%s] %s take %dms" +
                        "\nHeader [%s]/ Params: [%s]" +
                        "\nRequest Body: %s" +
                        "\nResponse Body %s" +
                        "\n===",
                transid, method, url, (System.currentTimeMillis() - start),
                headers, params,
                requestBody, responseBody);
        logger.info(ConsoleK.getColorMessage(ConsoleK.ANSI_BLUE, log));
    }

    public String put(String transid, String url, String txt,
                      ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        long start = System.currentTimeMillis();
        PutResult putResult = putRs(transid, url, txt, params, headers);
        String body = putResult.getBody();
        logInfoRequest(transid, url, "PUT", txt, start, headers, params, body);
        return body;
    }

    public String delete(String transid, String url,
                         ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        long start = System.currentTimeMillis();
        DeleteResult putResult = deleteRs(transid, url, params, headers);
        String body = putResult.getBody();
        logInfoRequest(transid, url, "DELETE", "Not Request Body", start, headers, params, body);
        return body;
    }

    public DeleteResult deleteRs(String transid, String url,
                                 ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {

        DeleteMethod method = new DeleteMethod();
        try {
            url = url.trim();
            if (!url.contains("?")) {
                url += "?";
            }
            for (int i = 0; params != null && i < params.size(); i++) {
                NameValuePair nvp = params.get(i);
                // method.addParameter(nvp);
                url += "&" + nvp.getName()
                        + "=" + URLEncoder.encode(nvp.getValue(), "UTF-8");
            }
            for (int i = 0; headers != null && i < headers.size(); i++) {
                NameValuePair nvp = headers.get(i);
                method.addRequestHeader(nvp.getName(), nvp.getValue());
            }
            method.setURI(new URI(url));
            httpClient.executeMethod(method);

            if (method.getStatusCode() >= 200 && method.getStatusCode() < 300) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                DeleteResult httpResult = new DeleteResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", POST Redirect, url = " + url
                        + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(POST) fail, url = " + url
                        + "; request = " + "" + "; respCode = " + method.getStatusCode());
            }
        } catch (Throwable e) {
            logger.info(transid + ", Exception: " + e.getMessage(), e);
        } finally {
            method.releaseConnection();
        }
        DeleteResult httpResult = new DeleteResult(method, null);
        return httpResult;
    }

    public PutResult putRs(String transid, String url, String txt,
                           ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {

        PutMethod method = new PutMethod();
        try {
            url = url.trim();
            if (!url.contains("?")) {
                url += "?";
            }
            ByteArrayRequestEntity entity = new ByteArrayRequestEntity(txt.getBytes("UTF-8"));
            for (int i = 0; params != null && i < params.size(); i++) {
                NameValuePair nvp = params.get(i);
                // method.addParameter(nvp);
                url += "&" + nvp.getName()
                        + "=" + URLEncoder.encode(nvp.getValue(), "UTF-8");
            }
            for (int i = 0; headers != null && i < headers.size(); i++) {
                NameValuePair nvp = headers.get(i);
                method.addRequestHeader(nvp.getName(), nvp.getValue());
            }
            method.setURI(new URI(url));
            method.setRequestEntity(entity);
            httpClient.executeMethod(method);

            if (method.getStatusCode() >= 200 && method.getStatusCode() < 300) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                PutResult httpResult = new PutResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", POST Redirect, url = " + url
                        + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(POST) fail, url = " + url
                        + "; request = " + txt + "; respCode = " + method.getStatusCode());
            }
        } catch (Throwable e) {
            logger.info(transid + ", Exception: " + e.getMessage(), e);
        } finally {
            method.releaseConnection();
        }
        PutResult httpResult = new PutResult(method, null);
        return httpResult;
    }

    public PostResult postRs(String transid, String url, String txt,
                             ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers, int retry, int retryCount) {

        PostMethod method = new PostMethod();
        try {
            url = url.trim();
            if (!url.contains("?")) {
                url += "?";
            }
            ByteArrayRequestEntity entity = new ByteArrayRequestEntity(txt.getBytes("UTF-8"));
            for (int i = 0; params != null && i < params.size(); i++) {
                NameValuePair nvp = params.get(i);
                // method.addParameter(nvp);
                url += "&" + nvp.getName()
                        + "=" + URLEncoder.encode(nvp.getValue(), "UTF-8");
            }
            for (int i = 0; headers != null && i < headers.size(); i++) {
                NameValuePair nvp = headers.get(i);
                method.addRequestHeader(nvp.getName(), nvp.getValue());
            }
            method.setURI(new URI(url));
            method.setRequestEntity(entity);
            httpClient.executeMethod(method);

            if (method.getStatusCode() >= 200 && method.getStatusCode() < 300) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                PostResult httpResult = new PostResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", POST Redirect, url = " + url
                        + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(POST) fail, url = " + url
                        + "; request = " + txt + "; respCode = " + method.getStatusCode());
            }
        } catch (Throwable e) {
            logger.info(transid + ", Exception: " + e.getMessage(), e);
        } finally {
            method.releaseConnection();
            if (retryCount < retry) {
                return postRs(transid, url, txt, params, headers, retry, ++retryCount);
            }
        }
        PostResult httpResult = new PostResult(method, null);
        return httpResult;
    }

    public String post2(String transid, String url, String txt,
                        ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        return postRs2(transid, url, txt, params, headers).getBody();
    }

    public PostResult postRs2(String transid, String url, String txt,
                              ArrayList<NameValuePair> params, ArrayList<NameValuePair> headers) {
        PostMethod method = new PostMethod();
        try {
            ByteArrayRequestEntity entity = new ByteArrayRequestEntity(txt.getBytes("UTF-8"));
            NameValuePair paramsArr[] = null;
            if (params != null) {
                paramsArr = new NameValuePair[params.size()];
                params.toArray(paramsArr);
            }
            for (int i = 0; headers != null && i < headers.size(); i++) {
                NameValuePair nvp = headers.get(i);
                method.addRequestHeader(nvp.getName(), nvp.getValue());
            }
            // logger.info(transid + ", PostURL: " + url);
            method.setURI(new URI(url));

            method.setRequestEntity(entity);
            if (paramsArr != null) {
                method.setRequestBody(paramsArr);
            }
            httpClient.executeMethod(method);

            if (method.getStatusCode() == HttpStatus.SC_OK) {
                String response = BaseUtils.readInputStream(method.getResponseBodyAsStream());
                PostResult httpResult = new PostResult(method, response);
                return httpResult;

            } else if (method.getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                logger.info(transid + ", POST Redirect, url = " + url
                        + "/ Redirect to: " + method.getResponseHeader("Location"));
            } else {
                logger.info(transid + ", Request(POST-2) fail, url = " + url
                        + "; request = " + txt + "; respCode = " + method.getStatusCode());
            }
        } catch (Throwable e) {
            logger.info(transid + ", URL = " + url + "; Exception: " + e.getMessage(), e);
        } finally {
            method.releaseConnection();
        }
        PostResult httpResult = new PostResult(method, null);
        return httpResult;
    }

    public static String parseData(String xml, String xmlTag) {
        return HttpServerUtils.parseData(xml, xmlTag);
    }

    public static String parseListParams2Str(String transid, ArrayList<NameValuePair> list) {
        String res = "";
        for (int i = 0; list != null && i < list.size(); i++) {
            NameValuePair bean = list.get(i);
            res += "&" + bean.getName() + "=" + bean.getValue();
        }
        res = res.replaceFirst("&", "");
        return res;
    }

    public static String postDefault(String transid, int timeout, String targetURL,
                                     String body, StringBuilder sbDesc) {
        HttpURLConnection httpConnection = null;
        PrintStream ps = null;
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URL sendUrl = new URL(targetURL);

            URLConnection urlCon = sendUrl.openConnection();
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            httpConnection = (HttpURLConnection) urlCon;

            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);

            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConnection.setRequestProperty("Content-Length", Integer.toString(body.length()));

            ps = new PrintStream(httpConnection.getOutputStream(), true, "utf-8");
            ps.write(body.getBytes("utf-8"));
            ps.flush();

            String str = httpConnection.getResponseMessage();

            is = httpConnection.getInputStream();
            isr = new InputStreamReader(is, "utf-8");
            br = new BufferedReader(isr);

            str = "";
            StringBuilder sb = new StringBuilder();
            while ((str = br.readLine()) != null) {
                sb.append(str + "\n");
            }
            String response = sb.toString().trim();
            return response;
        } catch (Exception e) {
            if (sbDesc != null) {
                sbDesc.append(String.valueOf(e.getMessage()).replace("\n", "-"));
            }
            Log4jLoader.getLogger().info(transid + ", url = "
                    + targetURL + ", Exception: " + e.getMessage(), e);
        } finally {
            try {
                br.close();
            } catch (Exception e) {
            }
            try {
                isr.close();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                ps.close();
            } catch (Exception e) {
            }
            try {
                // httpConnection.disconnect();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public static String postDefault(String transid, int timeout, String targetURL, String body) {
        ArrayList<NameValuePair> headers = null;
        return postDefault(transid, timeout, targetURL, body, headers);
    }

    public static String postDefault(String transid, int timeout, String targetURL, String body, ArrayList<NameValuePair> headers) {
        HttpURLConnection httpConnection = null;
        PrintStream ps = null;
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URL sendUrl = new URL(targetURL);

            URLConnection urlCon = sendUrl.openConnection();
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            httpConnection = (HttpURLConnection) urlCon;

            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);

            httpConnection.setRequestMethod("POST");
            httpConnection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConnection.setRequestProperty("Content-Length", Integer.toString(body.length()));

            if (headers != null && headers.size() > 0) {
                for (NameValuePair b : headers) {
                    httpConnection.setRequestProperty(b.getName(), b.getValue());
                }
            }
            ps = new PrintStream(httpConnection.getOutputStream(), true, "utf-8");
            ps.write(body.getBytes("utf-8"));
            ps.flush();

            String str = httpConnection.getResponseMessage();

            is = httpConnection.getInputStream();
            isr = new InputStreamReader(is, "utf-8");
            br = new BufferedReader(isr);

            str = "";
            StringBuilder sb = new StringBuilder();
            while ((str = br.readLine()) != null) {
                sb.append(str + "\n");
            }
            String response = sb.toString().trim();
            return response;
        } catch (Exception e) {
            Log4jLoader.getLogger().info(transid + ", url = " + targetURL
                    + ", Exception: " + e.getMessage(), e);
        } finally {
            try {
                br.close();
            } catch (Exception e) {
            }
            try {
                isr.close();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                ps.close();
            } catch (Exception e) {
            }
            try {
                httpConnection.disconnect();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public static String getDefault(String transid, String urlToRead, int timeout, StringBuilder sbDesc) {
        HttpURLConnection httpConnection = null;
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URL url = new URL(urlToRead);
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("GET");

            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);

            is = httpConnection.getInputStream();
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);

            String line = null;
            StringBuilder result = new StringBuilder();

            while ((line = br.readLine()) != null) {
                result.append(line + "\n");
            }
            return result.toString().trim();

        } catch (Exception e) {
            if (sbDesc != null) {
                sbDesc.append(String.valueOf(e.getMessage()).replace("\n", "-"));
            }
            Log4jLoader.getLogger().info(transid + ", url = " + urlToRead
                    + ", Exception: " + e.getMessage(), e);
        } finally {
            try {
                br.close();
            } catch (Exception e) {
            }
            try {
                isr.close();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                // httpConnection.disconnect();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public static String getDefault(String transid, String urlToRead, int timeout) {
        ArrayList<NameValuePair> b = null;
        return getDefault(transid, urlToRead, timeout, b);
    }

    public static String getDefault(String transid, String urlToRead, int timeout, ArrayList<NameValuePair> headers) {
        HttpURLConnection httpConnection = null;
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            URL url = new URL(urlToRead);
            httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestMethod("GET");

            if (headers != null && headers.size() > 0) {
                for (NameValuePair b : headers) {
                    httpConnection.setRequestProperty(b.getName(), b.getValue());
                }
            }
            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);

            is = httpConnection.getInputStream();
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);

            String line = null;
            StringBuilder result = new StringBuilder();

            while ((line = br.readLine()) != null) {
                result.append(line + "\n");
            }
            return result.toString().trim();

        } catch (Exception e) {
            Log4jLoader.getLogger().info(transid + ", url = " + urlToRead
                    + ", Exception: " + e.getMessage(), e);
        } finally {
            try {
                br.close();
            } catch (Exception e) {
            }
            try {
                isr.close();
            } catch (Exception e) {
            }
            try {
                is.close();
            } catch (Exception e) {
            }
            try {
                httpConnection.disconnect();
            } catch (Exception e) {
            }
        }
        return null;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }
}
