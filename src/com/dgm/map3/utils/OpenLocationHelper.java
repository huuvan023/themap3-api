package com.dgm.map3.utils;


import com.dgm.map3.model.exception.RestApiException;
import com.google.common.collect.Lists;
import com.ligerdev.appbase.utils.textbase.StringGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 04/20/22
 */
public class OpenLocationHelper {
    public static final String CODE_ALPHABET = "23456789CFGHJMPQRVWX";
    public static HashMap<String, String> stringStringHashMap = new HashMap<>();

    static Map<String,String> mapExtendCode = new HashMap<>();
    static {
        mapExtendCode.put("22","55");
        mapExtendCode.put("62","95");
        mapExtendCode.put("C2","H5");
        mapExtendCode.put("J2","Q5");
        mapExtendCode.put("R2","X5");


        mapExtendCode.put("26","59");
        mapExtendCode.put("66","99");
        mapExtendCode.put("C6","H9");
        mapExtendCode.put("J6","Q9");
        mapExtendCode.put("R6","X9");

        mapExtendCode.put("2C","5H");
        mapExtendCode.put("6C","9H");
        mapExtendCode.put("CC","HH");
        mapExtendCode.put("JC","QH");
        mapExtendCode.put("RC","XH");

        mapExtendCode.put("2J","5Q");
        mapExtendCode.put("6J","9Q");
        mapExtendCode.put("CJ","HQ");
        mapExtendCode.put("JJ","QQ");
        mapExtendCode.put("RJ","XQ");

        mapExtendCode.put("2R","5X");
        mapExtendCode.put("6R","9X");
        mapExtendCode.put("CR","HX");
        mapExtendCode.put("JR","QX");
        mapExtendCode.put("RR","XX");

    }

    public static List<String> getListGrid(String codeBottomLeft,String codeTopRight){

        codeBottomLeft=codeBottomLeft.split("_")[0];
        codeTopRight=codeTopRight.split("_")[0];

        String codeBottomRight = getCodeBottomRight(codeBottomLeft, codeTopRight);

        String currentData = codeBottomLeft;
        List<String> ListBottom = new ArrayList<>();
        int countCheck =0;
        ListBottom.add(codeBottomLeft);
        while (!currentData.equals(codeBottomRight)) {
            currentData = genGang(currentData);
            ListBottom.add(currentData);
            countCheck++;
            if (countCheck>=500)
                throw new RestApiException(4004,"DATA_TOO_MANY");
        }


        List<String> result = new ArrayList<>();
        for (String item : ListBottom) {

            result.add(item+"_"+mapExtendCode.get(item.substring(item.length()-2)));
            String codeTopLeft = getCodeTopLeft(item, codeTopRight);
            String currentDataItem =item;
            countCheck=0;
            while (!currentDataItem.equals(codeTopLeft)) {
                currentDataItem = genLen(currentDataItem);
                result.add(currentDataItem+"_"+mapExtendCode.get(currentDataItem.substring(currentDataItem.length()-2)));
                countCheck++;
                if (countCheck>=500)
                    throw new RestApiException(4004,"DATA_TOO_MANY");
            }
        }

         return result;
    }




    private static String getCodeBottomRight(String codeBottomLeft, String codeTopRight) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < codeBottomLeft.length(); i++) {
            if (i % 2 == 0) {
                stringBuilder.append(codeBottomLeft.charAt(i));
            } else {
                stringBuilder.append(codeTopRight.charAt(i));

            }
        }
        return stringBuilder.toString();
    }
    public static void generateMapLevel8Inside() {
        List<String> codeAlphabets = new ArrayList<>();
        for (char c : CODE_ALPHABET.toCharArray()) {
            codeAlphabets.add(c + "");
        }
        List<List<String>> alphabetSplit = Lists.partition(codeAlphabets, 4);
        List<String> listGroupLevel8 = new ArrayList<>();
        for (List<String> strings : alphabetSplit) {
            String firstChar = strings.get(0);
            String fourthChar = strings.get(3);
            for (List<String> list : alphabetSplit) {
                String firstCharTurn2 = list.get(0);
                String fourthCharTurn2 = list.get(3);
                String groupLevel8 = firstChar + "" + firstCharTurn2 + "_" + fourthChar + "" + fourthCharTurn2;
                stringStringHashMap.put(strings.get(0) + list.get(0), groupLevel8);
                stringStringHashMap.put(strings.get(0) + list.get(1), groupLevel8);
                stringStringHashMap.put(strings.get(0) + list.get(2), groupLevel8);
                stringStringHashMap.put(strings.get(0) + list.get(3), groupLevel8);
                stringStringHashMap.put(strings.get(1) + list.get(0), groupLevel8);
                stringStringHashMap.put(strings.get(1) + list.get(1), groupLevel8);
                stringStringHashMap.put(strings.get(1) + list.get(2), groupLevel8);
                stringStringHashMap.put(strings.get(1) + list.get(3), groupLevel8);
                stringStringHashMap.put(strings.get(2) + list.get(0), groupLevel8);
                stringStringHashMap.put(strings.get(2) + list.get(1), groupLevel8);
                stringStringHashMap.put(strings.get(2) + list.get(2), groupLevel8);
                stringStringHashMap.put(strings.get(2) + list.get(3), groupLevel8);
                stringStringHashMap.put(strings.get(3) + list.get(0), groupLevel8);
                stringStringHashMap.put(strings.get(3) + list.get(1), groupLevel8);
                stringStringHashMap.put(strings.get(3) + list.get(2), groupLevel8);
                stringStringHashMap.put(strings.get(3) + list.get(3), groupLevel8);
                listGroupLevel8.add(groupLevel8);
            }
        }
    }
    private static String getCodeTopLeft(String codeBottomLeft, String codeTopRight) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < codeBottomLeft.length(); i++) {
            if (i % 2 == 0) {
                stringBuilder.append(codeTopRight.charAt(i));
            } else {
                stringBuilder.append(codeBottomLeft.charAt(i));

            }
        }
        return stringBuilder.toString();
    }

    public static String getGridCodeFromLatLng(Double lat, Double lng) {
        OpenLocationCode openLocationCode = new OpenLocationCode(lat, lng, 8);
        String code = openLocationCode.getCode();
        String codeLevel8 = code.substring(6, 8);
        String codeLevel8Boundary = stringStringHashMap.get(codeLevel8);
        String codeLevel8Group = code.substring(0, 6).concat(codeLevel8Boundary);
        return codeLevel8Group;
    }

    private static String genLen(String a) {
        char lastX = a.charAt(a.length() - 2);
        if ((lastX == 'R' && a.length() == 8) || (lastX == 'X' && a.length() != 8)) {
            return genLen(a.substring(0, a.length() - 2)) + "2" + a.charAt(a.length() - 1);
        } else {

            int indexSum = 1;
            if (a.length() == 8)
                indexSum = 4;

            StringBuilder result = new StringBuilder(a);
            result.setCharAt(a.length() - 2, CODE_ALPHABET.charAt(CODE_ALPHABET.indexOf(lastX) + indexSum));

            return result.toString();
        }
    }

    private static String genGang(String a) {
        char lastY = a.charAt(a.length() - 1);
        if ((lastY == 'R' && a.length() == 8) || (lastY == 'X' && a.length() != 8)) {
            return genLen(a.substring(0, a.length() - 1)) + "2";
        } else {

            int indexSum = 1;
            if (a.length() == 8)
                indexSum = 4;

            StringBuilder result = new StringBuilder(a);
            result.setCharAt(a.length() - 1, CODE_ALPHABET.charAt(CODE_ALPHABET.indexOf(lastY) + indexSum));

            return result.toString();
        }
    }

}
