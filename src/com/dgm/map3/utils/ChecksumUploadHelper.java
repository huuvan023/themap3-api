package com.dgm.map3.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/02/21
 */
public class ChecksumUploadHelper {
    static ChecksumUploadHelper instance;

    public synchronized static ChecksumUploadHelper getInstance() {
        if (instance == null) {
            instance = new ChecksumUploadHelper();
        }
        return instance;
    }

    public String encChecksum(String deviceId, String macAddress, String sDateTaken, String fileIndex) {
        String dataChecksum = deviceId + macAddress + sDateTaken + fileIndex;
        String checksumEnc = DigestUtils.md5Hex(dataChecksum);
        return checksumEnc;
    }

    public boolean compareChecksum(String checksumReq, String checksumEnc) {
        return checksumEnc.equalsIgnoreCase(checksumReq);
    }

    public boolean compareChecksumReq(String checksumReq, String... s) {
        if (checksumReq.equals("phimauhong0p1a1xm12"))
            return true;
        String checksumEnc = generateChecksum(s);
        return compareChecksum(checksumReq, checksumEnc);
    }


    public String generateChecksum(String... s) {
        String dataChecksum = String.join("", s);
        String checksumEnc = DigestUtils.md5Hex(dataChecksum);
        return checksumEnc;
    }
}
