package com.dgm.map3.api.cmd;

import com.tdx.http.server.cmd.AbsAPIBaseCmd;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 12/24/20
 */
public class UploadTestCmd extends AbsAPIBaseCmd {
    private InputStream fileInputStream;
    private FormDataContentDisposition fileMetaData;

    @Override
    protected boolean authenticate() {
        return true;
    }

    @Override
    protected boolean checkRole() {
        return true;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public void setFileMetaData(FormDataContentDisposition fileMetaData) {
        this.fileMetaData = fileMetaData;
    }

    public UploadTestCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected void executeCmd() throws Exception {
        System.out.println(requestBody);
    }
}
