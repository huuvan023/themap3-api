package com.dgm.map3.api.cmd.user;


import com.dgm.map3.api.cmd.AbsAPIUserCmd;
import com.dgm.map3.api.msg.resp.RESP_UserInfo;
import com.dgm.map3.core.manage.UserManager;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tdx.http.server.ResultTypes;

import javax.servlet.http.HttpServletRequest;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/14/20
 */
public class GetUserInfoCmd extends AbsAPIUserCmd {
    public GetUserInfoCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected void executeCmd() throws Exception {
        UserEntity userEntity = UserManager.getUserEntityByUserName(userByToken.getUsername());
        if (userEntity == null) {
            setResultTypes(ResultTypes.SYSTEM_BUSY);
            return;
        }
        RESP_UserInfo respUserInfo = convertRESPByUserEntity(userEntity);
        setResponseData(respUserInfo);
    }


}
