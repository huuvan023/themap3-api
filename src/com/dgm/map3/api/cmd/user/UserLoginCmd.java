package com.dgm.map3.api.cmd.user;


import com.dgm.map3.api.msg.req.REQ_Login;
import com.dgm.map3.api.msg.resp.RESP_Login;
import com.dgm.map3.core.constant.UserConst;
import com.dgm.map3.model.db.cmd.user.DBUserLoginCmd;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tdx.authen.JWTUtil;
import com.tdx.authen.UserByToken;
import com.tdx.http.server.ResultTypes;
import com.tdx.http.server.cmd.AbsAPIBaseCmd;
import com.tdx.text.encrypt.EncodeUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public class UserLoginCmd extends AbsAPIBaseCmd {
    public UserLoginCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected boolean authenticate() {
        return true;
    }

    @Override
    protected boolean checkRole() {
        return true;
    }

    static JWTUtil jwtUtil = new JWTUtil();

    @Override
    protected void executeCmd() throws Exception {
        REQ_Login reqLogin = parseJSONRequest(REQ_Login.class);
        if (reqLogin == null) {
            return;
        }
        if (badRequestOnNullOrEmpty(reqLogin.getUsername(), reqLogin.getPassword())) {
            return;
        }

        String passwordEnc = EncodeUtils.encodePassword(reqLogin.getPassword());
        DBUserLoginCmd dbUserLoginCmd = new DBUserLoginCmd(channel, transid, reqLogin.getUsername(), passwordEnc);
        dbUserLoginCmd.exec();
        if (resultTypesIsNOK(dbUserLoginCmd.getResultTypes())) {
            return;
        }

        UserEntity userEntity = dbUserLoginCmd.getUserEntity();

        if (userEntity.getStatus() != UserConst.USER_STATUS_ACTIVE) {
            setResultTypes(ResultTypes.USER_INACTIVE);
            return;
        }


        UserByToken userByToken = new UserByToken();

        userByToken.setUid(userEntity.getId());
        userByToken.setUuid(userEntity.getUuid());
        userByToken.setUsername(userEntity.getUsername());
        userByToken.setPassword(userEntity.getPassword());
        long now = System.currentTimeMillis();
        userByToken.setLoginTime(now);
        userByToken.setRenewTime(now);
        String userTokenJson = gson.toJson(userByToken);

        String token = jwtUtil.encode("CORE", userTokenJson);

        setResponseData(new RESP_Login(token));
    }

}
