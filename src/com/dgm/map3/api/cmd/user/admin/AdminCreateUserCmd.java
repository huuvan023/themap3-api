package com.dgm.map3.api.cmd.user.admin;


import com.dgm.map3.api.cmd.AbsAPIUserAcceptRoleCmd;
import com.dgm.map3.api.msg.req.REQ_CreateUser;
import com.dgm.map3.api.msg.resp.RESP_UserInfo;
import com.dgm.map3.core.constant.UserConst;
import com.dgm.map3.core.manage.UserManager;
import com.dgm.map3.model.db.cmd.user.admin.DBCreateUserCmd;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tdx.http.server.ResultTypes;
import com.tdx.text.encrypt.EncodeUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/19/20
 */
public class AdminCreateUserCmd extends AbsAPIUserAcceptRoleCmd {
    public AdminCreateUserCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    public List<Integer> acceptRoles() {
        return Arrays.asList(UserConst.ADMIN);
    }

    @Override
    protected void executeCmd() throws Exception {
        REQ_CreateUser reqCreateUser = parseJSONRequest(REQ_CreateUser.class);
        if (reqCreateUser == null) {
            return;
        }
        if (badRequestOnNullOrEmptyObjects(
                reqCreateUser.getUsername(),
                reqCreateUser.getPassword(),
                reqCreateUser.getRoleId(),
                reqCreateUser.getFullname())) {
            return;
        }
        if (UserManager.existByUserName(reqCreateUser.getUsername())) {
            setResultTypes(ResultTypes.USER_EXISTED);
            return;
        }
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(reqCreateUser.getUsername());
        userEntity.setRoleId(reqCreateUser.getRoleId());
        userEntity.setFullname(reqCreateUser.getFullname());
        userEntity.setUuid(EncodeUtils.getRandomUUID());
        userEntity.setType(1);
        userEntity.setStatus(UserConst.USER_STATUS_ACTIVE);

        userEntity.setPassword(EncodeUtils.encodePassword(reqCreateUser.getPassword()));

        DBCreateUserCmd dbCreateUserCmd = new DBCreateUserCmd(transid, channel, userEntity);
        dbCreateUserCmd.exec();
        if (resultTypesIsNOK(dbCreateUserCmd.getResultTypes())) {
            return;
        }
        UserEntity userEntitySaved = dbCreateUserCmd.getUserEntity();
        RESP_UserInfo resp_userInfo = convertRESPByUserEntity(userEntitySaved);
        setResponseData(resp_userInfo);
        UserManager.putUser(userEntitySaved);
        UserManager.syncUserFromDB();
    }
}
