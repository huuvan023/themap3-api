package com.dgm.map3.api.cmd.user.admin;


import com.dgm.map3.api.cmd.AbsAPIUserAcceptRoleCmd;
import com.dgm.map3.api.msg.resp.RESP_UserInfo;
import com.dgm.map3.core.constant.UserConst;
import com.dgm.map3.core.manage.UserManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/19/20
 */
public class AdminGetListUserCmd extends AbsAPIUserAcceptRoleCmd {
    public AdminGetListUserCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    public List<Integer> acceptRoles() {
        return Arrays.asList(UserConst.MODERATOR, UserConst.ADMIN);
    }


    @Override
    protected void executeCmd() throws Exception {

        Integer userId =null;
        List<RESP_UserInfo> getUsers=null;
        getUsers=  UserManager.getListUser()
                .stream()
                .map(this::convertRESPByUserEntity)
                .collect(Collectors.toList());
        setResponseDataBasicList(getUsers);
    }
}
