package com.dgm.map3.api.cmd;

import com.dgm.map3.api.msg.resp.RESP_UserInfo;
import com.dgm.map3.core.constant.UserConst;
import com.dgm.map3.core.manage.UserManager;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tbv.utils.textbase.StringUtil;
import com.tdx.authen.JWTUtil;
import com.tdx.authen.UserByToken;
import com.tdx.http.server.cmd.AbsAPIBaseCmd;

import javax.servlet.http.HttpServletRequest;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public class AbsAPIUserCmd extends AbsAPIBaseCmd {
    protected static JWTUtil jwtUtil = new JWTUtil();
    protected UserByToken userByToken;
    protected UserEntity userEntity;

    public AbsAPIUserCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    protected Integer getUserRole(String userName) {
        UserEntity user = UserManager.getUserEntityByUserName(userName);
        return user.getRoleId();
    }

    protected Integer getUserRole() {
        Integer userRole = getUserRole(userByToken.getUsername());
        return  userRole;
    }

    protected Integer getUserID() {
        return  userByToken.getUid();
    }

    @Override
    protected boolean authenticate() {
        if (StringUtil.isNullOrEmpty(token)) {
            return false;
        }
        String tokenData = jwtUtil.decodeJWT2S(token);
        if(StringUtil.isNullOrEmpty(tokenData)) return false;

        userByToken = gson.fromJson(tokenData, UserByToken.class);

        if(userByToken == null) {
            return false;
        }

        userEntity = UserManager.getUserEntityByUserName(userByToken.getUsername());

        if(userEntity == null) {
            return false;
        }

        if(userEntity.getStatus() != UserConst.USER_STATUS_ACTIVE) {
            return false;
        }

        if (userByToken.getPassword() == null) {
            return false;
        }

        if (!userByToken.getPassword().equals(userEntity.getPassword())) {
            return false;
        }

        return true;
    }

    @Override
    protected boolean checkRole() {
        return true;
    }

    @Override
    protected void executeCmd() throws Exception {

    }
    protected RESP_UserInfo convertRESPByUserEntity(UserEntity userEntity) {
        return RESP_UserInfo.convertRESPByUserEntity(userEntity);
    }
}
