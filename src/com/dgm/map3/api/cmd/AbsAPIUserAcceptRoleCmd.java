package com.dgm.map3.api.cmd;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public abstract class AbsAPIUserAcceptRoleCmd extends AbsAPIUserCmd{
    public AbsAPIUserAcceptRoleCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected boolean checkRole() {
        Integer userRole = getUserRole();
        if(userRole == null) {
            logger.info("RoleId is null by user = " + userByToken);
            return false;
        }
        if(!acceptRoles().contains(userRole)) {
            return false;
        }
        return true;
    }
    public abstract List<Integer> acceptRoles();
    protected List<Integer> getRoles() {
        return acceptRoles();
    }
}
