package com.dgm.map3.api.cmd.define;

import com.dgm.map3.core.manage.DefineManager;
import com.dgm.map3.model.entity.define.ServerResourcesEntity;
import com.tdx.http.server.cmd.AbsAPIBaseCmd;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 3/08/22
 */
public class GetServerResourceCmd extends AbsAPIBaseCmd {
    public GetServerResourceCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected boolean authenticate() {
        return true;
    }

    @Override
    protected boolean checkRole() {
        return true;
    }

    @Override
    protected void executeCmd() throws Exception {
        List<ServerResourcesEntity> serverResourcesEntities;
        serverResourcesEntities = DefineManager.getAllServerResource();

        setResponseData(serverResourcesEntities);
    }

}
