package com.dgm.map3.api.cmd.define;

import com.dgm.map3.api.cmd.AbsAPIUserCmd;
import com.dgm.map3.model.db.cmd.define.DBGetIncidentTypeCmd;

import javax.servlet.http.HttpServletRequest;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 01/06/21
 */
public class GetIncidentTypeCmd extends AbsAPIUserCmd {
    public GetIncidentTypeCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected void executeCmd() throws Exception {
        DBGetIncidentTypeCmd dbGetIncidentTypeCmd = new DBGetIncidentTypeCmd(transid, channel);
        dbGetIncidentTypeCmd.exec();
        setResponseData(dbGetIncidentTypeCmd.getIncidentTypeDefine());
    }
}
