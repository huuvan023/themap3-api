package com.dgm.map3.api.cmd.map;


import com.dgm.map3.api.msg.resp.RESP_GetGridInfo;
import com.dgm.map3.api.msg.resp.RESP_GetGridInfoContributorList;
import com.dgm.map3.model.db.cmd.map.DBGetGridInfoCmd;
import com.dgm.map3.model.entity.map.GridInfoEntity;
import com.tdx.http.server.cmd.AbsAPIBaseCmd;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 15/6/22
 */
public class GetMapInfoCmd extends AbsAPIBaseCmd {
    public GetMapInfoCmd(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    @Override
    protected boolean authenticate() {
        return true;
    }

    @Override
    protected boolean checkRole() {
        return true;
    }

    @Override
    protected void executeCmd() throws Exception {
        String gridId = httpServletRequest.getParameter("key");
        if(badRequestOnNullOrEmpty(gridId)) {
            return;
        }

        DBGetGridInfoCmd command = new DBGetGridInfoCmd(transid, channel, gridId);
        command.exec();

        List<GridInfoEntity> gridInfoEntityList = command.getGridInfo();
        RESP_GetGridInfo response = new RESP_GetGridInfo();

        if(gridInfoEntityList.size() > 0) {
            response.setStatus(gridInfoEntityList.get(0).getStatus());
            response.setGridKey(gridInfoEntityList.get(0).getGridKey());
        }

        Long totalGridRaw = Long.valueOf(0);
        List<RESP_GetGridInfoContributorList> contributorLists = new ArrayList<>();

        for (GridInfoEntity grid : gridInfoEntityList) {
            totalGridRaw += grid.getTotalImages();
            RESP_GetGridInfoContributorList contributor = new RESP_GetGridInfoContributorList();
            contributor.setEndDate(grid.getEndWorkingTime());
            contributor.setStartDate(grid.getStartWorkingTime());
            contributor.setTotalRawCollected(grid.getTotalImages());
            contributor.setFullName(grid.getCollectorFullname());
            contributor.setCollectorId(grid.getCollectorId());
            contributorLists.add(contributor);
        }

        response.setTotalRawCollected(totalGridRaw);
        response.setContributors(contributorLists);

        if(resultTypesIsNOK(command.getResultTypes())) {
            return;
        }
        setResponseData(response);
    }
}
