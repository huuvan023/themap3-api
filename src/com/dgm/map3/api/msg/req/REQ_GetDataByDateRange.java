package com.dgm.map3.api.msg.req;

import lombok.Data;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/12/20
 */
@Data
public class REQ_GetDataByDateRange {
    protected Date fromDate;
    protected Date toDate;
}
