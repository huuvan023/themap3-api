package com.dgm.map3.api.msg.req;

import lombok.Data;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
@Data
public class REQ_SubmitIncident {
    private String rawUuid;
    private Date createdTime;
    private Integer incidentType;
    private Date incidentTime;
    private Integer duration;
    private Integer numberRoadCross;
    private Integer laneBlock;
    private Double heading;
    private String roadName;
    private Double lat;
    private Double lng;
    private String additionalInformation;
}
