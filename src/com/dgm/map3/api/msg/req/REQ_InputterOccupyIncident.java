package com.dgm.map3.api.msg.req;

import lombok.Data;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 3/7/22
 */
@Data
public class REQ_InputterOccupyIncident extends BasePaginate {
    Integer id;
}
