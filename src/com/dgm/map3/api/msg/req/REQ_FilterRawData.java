package com.dgm.map3.api.msg.req;
import lombok.Data;

import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/19/20
 */
@Data
public class REQ_FilterRawData extends BasePaginate{
    private String rawDataId;
    private List<Integer> status;
    private String fromDate;
    private String toDate;
}