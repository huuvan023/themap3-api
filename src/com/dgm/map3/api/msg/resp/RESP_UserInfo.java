package com.dgm.map3.api.msg.resp;

import com.dgm.map3.model.entity.user.UserEntity;
import lombok.Data;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/19/20
 */
@Data
public class RESP_UserInfo {
    private Integer userId;
    private String uuid;
    private String username;
    private String password;
    private int type;
    private int roleId;
    private int status;
    private String fullname;
    private String email;
    private String phonenumber;
    private String address;
    private Integer gender;
    private String avatarUrl;
    private Date birthday;


    public static RESP_UserInfo convertRESPByUserEntity(UserEntity userEntity) {
        RESP_UserInfo respUserInfo = new RESP_UserInfo();
        respUserInfo.setUserId(userEntity.getId());
        respUserInfo.setUuid(userEntity.getUuid());
        respUserInfo.setUsername(userEntity.getUsername());
        respUserInfo.setPassword("**********");
        respUserInfo.setFullname(userEntity.getFullname());
        respUserInfo.setEmail(userEntity.getEmail());
        respUserInfo.setPhonenumber(userEntity.getPhonenumber());
        respUserInfo.setBirthday(userEntity.getBirthday());
        respUserInfo.setAddress(userEntity.getAddress());
        respUserInfo.setGender(userEntity.getGender());
        respUserInfo.setStatus(userEntity.getStatus());
        respUserInfo.setRoleId(userEntity.getRoleId());
        respUserInfo.setType(userEntity.getType());
        respUserInfo.setAvatarUrl(userEntity.getAvatarUrl());
        return respUserInfo;
    }
}
