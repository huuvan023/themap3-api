package com.dgm.map3.api.msg.resp;

import lombok.Data;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
@Data
public class RESP_Login {
    private String token;

    public RESP_Login(String token) {
        this.token = token;
    }
}
