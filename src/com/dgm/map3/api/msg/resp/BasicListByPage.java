package com.dgm.map3.api.msg.resp;

import lombok.Data;

import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/7/22
 */
@Data
public class BasicListByPage<T> {
    String scrollId;
    int totalRecord;
    List<T> data;
}
