package com.dgm.map3.api.msg.resp;

import lombok.Data;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
@Data
public class RESP_GetGridInfoContributorList {
    private Date startDate;
    private Date endDate;
    private long totalRawCollected;
    private String totalIncome;
    private String fullName;
    private Integer collectorId;
}
