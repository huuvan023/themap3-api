package com.dgm.map3.api.msg.resp;

import lombok.Data;

import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
@Data
public class RESP_GetGridInfo {
    private String gridKey;
    private long totalRawCollected;
    private String totalIncome;
    private String currencyUnit;
    private int status;
    private Integer collectorId;
    private Integer ownerId;
    List<RESP_GetGridInfoContributorList> contributors;
}

