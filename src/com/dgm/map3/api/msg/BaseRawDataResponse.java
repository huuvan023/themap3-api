package com.dgm.map3.api.msg;

import lombok.Data;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/08/21
 */
@Data
public class BaseRawDataResponse {
    private int id;
    private String uuid;
    private String imagePath;
    private Integer driverId;
    private Double lat;
    private Double lng;
    private Double heading;
    private Double altitude;
    private Double speed;
    private Date createdTime;
    private Date photoTakenTime;
    private int status;
    private Integer serverResourceId;
    private Integer imageType;
}
