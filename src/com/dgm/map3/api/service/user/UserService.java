package com.dgm.map3.api.service.user;


import com.dgm.map3.api.cmd.user.GetUserInfoCmd;
import com.dgm.map3.api.cmd.user.UserLoginCmd;
import com.dgm.map3.api.cmd.user.admin.AdminCreateUserCmd;
import com.dgm.map3.api.cmd.user.admin.AdminGetListUserCmd;
import com.tdx.http.server.service.BaseService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
@Path("user")
public class UserService extends BaseService {

    @POST
    @Path("login")
    public Response userLogin(String body) throws Exception {
        UserLoginCmd userLoginCmd = new UserLoginCmd(httpServletRequest);
        userLoginCmd.setRequestBody(body);
        userLoginCmd.execute();
        return userLoginCmd.getResponse();
    }
    @GET
    @Path("info")
    public Response getUserInfo() throws Exception {
        GetUserInfoCmd getUserInfoCmd = new GetUserInfoCmd(httpServletRequest);
        getUserInfoCmd.execute();
        return getUserInfoCmd.getResponse();
    }

    @POST
    @Path("admin/create")
    public Response adminCreateUserAccount(String body) throws Exception {
        AdminCreateUserCmd request = new AdminCreateUserCmd(httpServletRequest);
        request.setRequestBody(body);
        request.execute();
        return request.getResponse();
    }

}