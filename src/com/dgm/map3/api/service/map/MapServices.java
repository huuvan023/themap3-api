package com.dgm.map3.api.service.map;


import com.dgm.map3.api.cmd.map.GetMapInfoCmd;
import com.dgm.map3.api.cmd.user.GetUserInfoCmd;
import com.dgm.map3.api.cmd.user.UserLoginCmd;
import com.dgm.map3.api.cmd.user.admin.AdminCreateUserCmd;
import com.tdx.http.server.service.BaseService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 15/6/22
 */
@Path("grid")
public class MapServices extends BaseService {

    @GET
    @Path("")
    public Response userLogin(String body) throws Exception {
        GetMapInfoCmd command = new GetMapInfoCmd(httpServletRequest);
        command.execute();
        return command.getResponse();
    }

//    @GET
//    @Path("login")
//    public Response userLogin(String body) throws Exception {
//        UserLoginCmd userLoginCmd = new UserLoginCmd(httpServletRequest);
//        userLoginCmd.setRequestBody(body);
//        userLoginCmd.execute();
//        return userLoginCmd.getResponse();
//    }
}