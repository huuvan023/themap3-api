package com.dgm.map3.api.service.define;

import com.dgm.map3.api.cmd.define.GetIncidentTypeCmd;
import com.dgm.map3.api.cmd.define.GetServerResourceCmd;
import com.tdx.http.server.service.BaseService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("define")
public class DefineServices extends BaseService {

//    @POST
//    @Path("collector-submit")
//    public Response collectorSubmit(String body) throws Exception {
//        CollectorSubmitIncidentCmd request = new CollectorSubmitIncidentCmd(httpServletRequest);
//        request.setRequestBody(body);
//        request.execute();
//        return request.getResponse();
//    }
    @GET
    @Path("server-resource")
    public Response getServerResource(String body) throws Exception {
        GetServerResourceCmd request = new GetServerResourceCmd(httpServletRequest);
        request.execute();
        return request.getResponse();
    }
    @GET
    @Path("incident-type")
    public Response getDefineIncidentType() {
        GetIncidentTypeCmd request = new GetIncidentTypeCmd(httpServletRequest);
        request.execute();
        return request.getResponse();
    }
}
