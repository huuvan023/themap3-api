package com.dgm.map3.api.service;


import com.tdx.http.server.service.BaseService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/15/20
 */
@Path("")
public class GeneralService extends BaseService {
    @GET
    @Path("")
    public Response hi() throws Exception {
        return Response.ok("Hi").build();
    }
}
