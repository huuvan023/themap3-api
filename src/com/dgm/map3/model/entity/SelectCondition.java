package com.dgm.map3.model.entity;

import lombok.Data;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/08/22
 */
@Data
public class SelectCondition {
    String condition;
    String key;
    String operator;
    Object value;
    Object value2;
    boolean inGroup;

    public SelectCondition(String condition, String key, String operator, Object value) {
        this.condition = condition;
        this.key = key;
        this.operator = operator;
        this.value = value;
    }

    public SelectCondition(String condition, String key, String operator, Object value, boolean inGroup) {
        this.condition = condition;
        this.key = key;
        this.operator = operator;
        this.value = value;
        this.inGroup = inGroup;
    }

    public SelectCondition(String condition, String key, String operator, Object value1, Object value2) {
        this.condition = condition;
        this.operator = operator;
        this.key = key;
        this.value = value1;
        this.value2 = value2;
    }

    public static SelectCondition newCondition(String condition, String key, String operator, Object value) {
        return new SelectCondition(condition, key, operator, value);
    }

    public static SelectCondition newCondition(String condition, String key, String operator, Object value, Object value2) {
        return new SelectCondition(condition, key, operator, value, value2);
    }

    public static SelectCondition newConditionInGroup(String condition, String key, String operator, String value) {
        return new SelectCondition(condition, key, operator, value, true);
    }

    public String firstSQL() {
        return String.format(" %s %s %s ", key, operator, getValueByOperator());
    }

    public String getValueByOperator() {
        String valueByOperator = "";
        if(operator.equals("in")) {
            valueByOperator = String.format(" (%s) ", value);
        }
        else if (operator.equals("like")) {
            valueByOperator = "'%" + value + "%'";
        }
        else if (operator.equals("between")) {
            valueByOperator = String.format("'%s' and '%s'", value, value2);
        }
        else {
            valueByOperator = String.format("'%s'", value);
        }
        return valueByOperator;
    }

    public String toSQL() {
        return String.format(" %s %s %s %s", condition, key, operator, getValueByOperator());
    }
}
