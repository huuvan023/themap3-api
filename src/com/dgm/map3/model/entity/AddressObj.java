package com.dgm.map3.model.entity;

import lombok.Data;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/12/20
 */
@Data
public class AddressObj {
    private Integer provinceId;
    private Integer districtId;
    private Integer wardId;
    private String street;

}
