package com.dgm.map3.model;

import lombok.Data;

@Data
public class BoundingBox {
    public double north;
    public double south;
    public double east;
    public double west;

    public BoundingBox(double east, double south, double west, double north) {
        this.east = east;
        this.south = south;
        this.west = west;
        this.north = north;
    }
}