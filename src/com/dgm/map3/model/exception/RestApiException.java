package com.dgm.map3.model.exception;


public class RestApiException extends RuntimeException {

    private int errorCode;
    private String errorMsg;

    public RestApiException(ERROR exception) {
        super();
        this.errorCode = exception.getCode();
        this.errorMsg = exception.getMessage();
    }

    public RestApiException(int code, String errorMsg) {
        super();
        this.errorCode = code;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return "ApiException{" +
                "errorCode=" + errorCode +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }

}
