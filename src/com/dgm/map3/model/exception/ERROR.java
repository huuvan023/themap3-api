package com.dgm.map3.model.exception;

public enum ERROR {

    SUCCESS(0, "SUCCESS"),
    SYSTEM_ERROR(5000, "SYSTEM ERROR "),
    SYSTEM_MAINTAIN(5001, "SYSTEM MAINTAIN "),
    DB_INSERT_FAILED(5002, "DB INSERT FAILED "),
    DB_UPDATE_FAILED(5003, "DB INSERT FAILED "),

    INVALID_PARAM(4001, "INVALID PARAM "),
    BAD_CHECKSUM(4002, "Bad Checksum "),
    CHECKSUM_REQUIRED(4003, "Checksum required "),
    BAD_REQUEST(4004, "BAD REQUEST "),
    BAD_REQUEST_INVALID_BODY_STRUCT(40041, "BAD_REQUEST_INVALID_BODY_STRUCT "),
    BATCH_ID_REQUIRED(4005, "Batch ID required "),
    DATA_EXISTED(4006, "DATA EXISTED "),
    EMPTY_FILE(4007, "File is Zero "),
    REQUEST_TOO_FAST(4008, "LIGHT SPEED? "),
    ID_HAS_ASSIGNED(4009, "Data has assigned "),
    FILE_NOT_VALID(4019, "File is invalid format"),
    SHAPE_INVALID(4020, " Shape has other user data "),
    DATA_PROCESSING(4021, " data processing "),
    NOT_SAME_DRIVER_ID(4022, " not same driver "),
    MAX_DATA_ASSIGN(4023, " Max data assign "),
    HAS_DATA_USING(4024, "box has data using "),
    TOO_LONG_DISTANCE(4025, "too long distance "),
    WRONG_DATA(4004, "wrong data "),
    HAS_CONFIRM_DATA(4030, "has confirm data "),
    GRID_EMPTY_DATA(4031, "Grid empty data "),
    MUST_DONE_CROSS_CHECK(4032, "Must done cross check "),
    MUST_SUBMIT_ALL_LABEL(4033, "Must submit all label "),


    USER_NOT_AUTHENTICATED(600, "USER NOT AUTHEN"),
    USER_NOT_EXISTED(601, "USER NOT EXISTED "),
    USER_EXISTED(602, "USER EXISTED "),
    USER_WRONG_PASSWORD(603, "WRONG PASSWORD "),
    USER_NOT_HAS_ROLE(604, "USER NOT HAS ROLE "),
    NOT_FOR_YOU(605, "Not For You !!"),
    USER_INACTIVE(606, "User inactive"),

    ;

    private int code;
    private String message;

    ERROR(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
