package com.dgm.map3.model.db.cmd.raw_data;

import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.map.GridInfoEntity;
import com.dgm.map3.model.entity.raw_data.CollectData;
import com.ligerdev.appbase.utils.db.BaseDAO;
import lombok.Getter;

import java.util.List;

public class DBGetCollectRawDataPageCmd extends AbsBaseDBCmd {
    List<CollectData> rawDataList;
    private int page;
    private int pageSize;
    private long lastId;

    public DBGetCollectRawDataPageCmd(String transid, String channel, int page, int pageSize, long lastId) {
        super(transid, channel);
        this.page = page;
        this.pageSize = pageSize;
        this.lastId = lastId;
    }

    public List<CollectData> getRawDataList() {
        return rawDataList;
    }

    @Override
    protected String getSQL() {
        int startIndex = calBeginIndex(page, pageSize);
        return "select * from collect.collect_data where lat !=0.0 and lng !=0.0  and id >"+lastId+"  order by id asc limit "+pageSize;
    }

    @Override
    protected void execute() throws Exception {
        if (baseDAO==null)
            baseDAO= BaseDAO.getInstance("main");
        rawDataList = baseDAO.getListBySql(transid, CollectData.class, getSQL(), null, null);

    }

    protected int calBeginIndex(int page, int pagesize) {
        int pageTemp = page;
        if (pageTemp <= 0)
            pageTemp = 1;
        int begin = (pageTemp - 1) * pagesize;
        return begin;
    }
}
