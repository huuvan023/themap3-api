package com.dgm.map3.model.db.cmd.raw_data;

import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.raw_data.CollectData;
import com.ligerdev.appbase.utils.db.BaseDAO;

import java.util.List;

public class DBUpdateGridKeyCmd extends AbsBaseDBCmd {
    String newGridKey;
    Long collectId;

    public DBUpdateGridKeyCmd(String transid, String channel, String newGridKey, Long collectId) {
        super(transid, channel);
        this.newGridKey = newGridKey;
        this.collectId = collectId;
    }

    @Override
    protected String getSQL() {
//        return "UPDATE collect.collect_data \n" +
//                "SET grid_id = g.id \n" +
//                "FROM collect.collect_data c INNER JOIN collect.grid_maps g \n" +
//                "ON g.grid_key = ? \n" +
//                "WHERE c.id = ?";
        return "update collect.collect_data as a\n" +
                "set grid_id= b.id \n" +
                "from  collect.grid_maps as b\n" +
                "where b.grid_key=? and a.id = ?";
    }

    @Override
    protected void execute() throws Exception {
        if (baseDAO==null)
            baseDAO= BaseDAO.getInstance("main");
        baseDAO.execSql(transid, getSQL(), newGridKey, collectId);
    }
}
