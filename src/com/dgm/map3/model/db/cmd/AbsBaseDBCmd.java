package com.dgm.map3.model.db.cmd;

import com.dgm.map3.core.manage.DBManager;
import com.ligerdev.appbase.utils.db.BaseDAO;
import com.tdx.http.server.BaseResultType;
import com.tdx.http.server.ResultTypes;
import org.apache.log4j.Logger;

import static org.apache.log4j.Logger.getLogger;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/05/20
 */
public abstract class AbsBaseDBCmd {
    protected Logger logger = getLogger(this.getClass());
    protected String transid;
    protected String channel;
    protected BaseDAO baseDAO;
    protected BaseResultType resultTypes = ResultTypes.OK;

    public BaseResultType getResultTypes() {
        return resultTypes;
    }

    public AbsBaseDBCmd(String transid, String channel) {
        this.transid = transid;
        this.channel = channel;
        baseDAO = DBManager.baseDAO;
    }

    protected void logInfo(String msg) {
        logger.info(String.format("AbsDB: channel=[%s], transid=[%s]: %s", transid, channel, msg));
    }

    protected abstract String getSQL();

    protected abstract void execute() throws Exception;

    public void exec()  {
        long start = System.currentTimeMillis();
        try {
            execute();
        } catch (Exception e) {
            setResultTypes(ResultTypes.UNKNOWN);
            logger.info("Exception when exec DB", e);
        } finally {
            logInfo(String.format("Execute take= %s (ms)", (System.currentTimeMillis() - start)));
        }
    }

    protected void setResultTypes(BaseResultType resultTypes) {
        this.resultTypes = resultTypes;
    }
}
