package com.dgm.map3.model.db.cmd.user;

import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.user.UserEntity;
import lombok.Getter;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class DBSyncUserCmd extends AbsBaseDBCmd {
    public DBSyncUserCmd(String transid, String channel) {
        super(transid, channel);
    }

    @Getter
    private ConcurrentHashMap<Integer, UserEntity> mapUserIdConcurrentHashMap;
    @Getter
    private ConcurrentHashMap<String, UserEntity> mapUserEntityConcurrentHashMap;
    @Getter
    private ConcurrentHashMap<String, String> mapUserNameConcurrentHashMapUUID;
    private List<UserEntity> userEntityList;

    @Override
    protected String getSQL() {
        return "SELECT * FROM user WHERE status IN(0,1,2) and type = 1";
    }
    @Override
    protected void execute() throws Exception {
        userEntityList = baseDAO.getListBySql(transid, UserEntity.class, getSQL(), null, null, null);
        mapUserEntityConcurrentHashMap = new ConcurrentHashMap<>();
        mapUserNameConcurrentHashMapUUID = new ConcurrentHashMap<>();
        mapUserIdConcurrentHashMap = new ConcurrentHashMap<>();

        for (UserEntity user: userEntityList) {
            mapUserIdConcurrentHashMap.put(user.getId(), user);
            mapUserEntityConcurrentHashMap.put(user.getUsername(), user);
            mapUserNameConcurrentHashMapUUID.put(user.getUuid(), user.getUsername());
        };
    }
}
