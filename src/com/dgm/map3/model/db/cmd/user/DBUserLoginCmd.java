package com.dgm.map3.model.db.cmd.user;


import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tdx.http.server.ResultTypes;

import java.util.Date;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public class DBUserLoginCmd extends AbsBaseDBCmd {
    private String username;
    private String password;
    private UserEntity userEntity;

    public DBUserLoginCmd(String transid, String channel, String username, String password) {
        super(transid, channel);
        this.username = username;
        this.password = password;
    }

    @Override
    protected String getSQL() {
        return "select * from `user` where username = ? and `password` = ?";
    }

    @Override
    protected void execute() throws Exception {
        boolean checkUserExist = baseDAO.hasResult(transid, "select 1 from user where username = ?", username);
        if (!checkUserExist) {
            setResultTypes(ResultTypes.USER_NOT_EXISTED);
            return;
        }
        userEntity = baseDAO.getBeanBySql(transid, UserEntity.class, getSQL(), username, password);
        if (userEntity == null) {
            setResultTypes(ResultTypes.USER_WRONG_PASSWORD);
            return;
        }
        userEntity.setLastLoginTime(new Date());
        baseDAO.updateBeanFields(transid, userEntity, true, "last_login_time");
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }
}
