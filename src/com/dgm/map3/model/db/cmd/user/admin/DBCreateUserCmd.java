package com.dgm.map3.model.db.cmd.user.admin;


import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.user.UserEntity;
import com.tdx.http.server.ResultTypes;

import java.util.Date;


/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/15/20
 */
public class DBCreateUserCmd extends AbsBaseDBCmd {
    private UserEntity userEntity;

    public DBCreateUserCmd(String transid, String channel, UserEntity userEntity) {
        super(transid, channel);
        this.userEntity = userEntity;
    }

    @Override
    protected String getSQL() {
        return null;
    }

    @Override
    protected void execute() throws Exception {
        userEntity.setCreatedTime(new Date());
        int rs = baseDAO.insertBean(transid, userEntity);
        if (rs <= 0) {
            setResultTypes(ResultTypes.DB_INSERT_FAILED);
        }
        userEntity.setId(rs);
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }
}
