package com.dgm.map3.model.db.cmd.map;

import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.map.GridInfoEntity;
import com.dgm.map3.model.entity.user.UserEntity;
import lombok.Getter;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class DBGetGridInfoCmd extends AbsBaseDBCmd {
    private String gridKey;
    @Getter
    private List<GridInfoEntity> gridInfo;
    public DBGetGridInfoCmd(String transid, String channel, String gridKey) {
        super(transid, channel);
        this.gridKey = gridKey;
    }
    @Override
    protected String getSQL() {
        return "SELECT g.grid_key as grid_key, " +
                " c.driver_id as collector_id, " +
                " u.fullname as collector_fullname, " +
                " g.status, " +
                " min(c.created_time) as start_working_time, " +
                "  max(c.created_time) as end_working_time, COUNT(*) as total_images " +
                "FROM collect.collect_data c inner join collect.grid_maps g on g.id = c.grid_id " +
                "inner join collect.user u on u.id = c.driver_id " +
                "WHERE g.grid_key = ? GROUP BY driver_id, g.status, g.grid_key, u.fullname";
    }
    @Override
    protected void execute() throws Exception {
        gridInfo = baseDAO.getListBySql(transid, GridInfoEntity.class, getSQL(), null, null, gridKey);
    }
}
