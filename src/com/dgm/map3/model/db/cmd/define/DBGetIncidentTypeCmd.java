package com.dgm.map3.model.db.cmd.define;


import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.define.IncidentTypeDefineEntity;
import lombok.Getter;

import java.util.List;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 17/09/21
 */
public class DBGetIncidentTypeCmd extends AbsBaseDBCmd {
    @Getter
    List<IncidentTypeDefineEntity> incidentTypeDefine;

    public DBGetIncidentTypeCmd(String transid, String channel) {
        super(transid, channel);
    }

    @Override
    protected String getSQL() {
        return "select * from incident_type_define";
    }

    @Override
    protected void execute() throws Exception {
        incidentTypeDefine = baseDAO.getListBySql(transid, IncidentTypeDefineEntity.class, getSQL(), null, null, null);
    }
}
