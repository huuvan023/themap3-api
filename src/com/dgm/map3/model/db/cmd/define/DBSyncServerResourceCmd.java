package com.dgm.map3.model.db.cmd.define;

import com.dgm.map3.model.db.cmd.AbsBaseDBCmd;
import com.dgm.map3.model.entity.define.ServerResourcesEntity;
import lombok.Getter;

import java.util.List;

public class DBSyncServerResourceCmd extends AbsBaseDBCmd {

    @Getter
    List<ServerResourcesEntity> data;

    public DBSyncServerResourceCmd(String transid, String channel) {
        super(transid, channel);
    }

    @Override
    protected String getSQL() {
        return "SELECT * FROM server_resources";
    }
    @Override
    protected void execute() throws Exception {
        data = baseDAO.getListBySql(transid, ServerResourcesEntity.class, getSQL(), null,null,null);
    }
}
