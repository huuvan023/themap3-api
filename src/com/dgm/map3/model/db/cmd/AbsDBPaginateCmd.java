package com.dgm.map3.model.db.cmd;

import com.dgm.map3.model.entity.SelectCondition;
import com.ligerdev.appbase.utils.db.BaseDAO;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbsDBPaginateCmd extends AbsBaseDBCmd{
    protected int page;
    protected int pageSize;
    protected int beginIndex;
    @Getter
    protected int totalRecord;

    public AbsDBPaginateCmd(String transid, String channel) {
        super(transid, channel);
    }

    public AbsDBPaginateCmd(String transid, String channel, int page, int pageSize) {
        super(transid, channel);
        this.page = page;
        this.pageSize = pageSize;
        beginIndex = calBeginIndex(page, pageSize);
    }

    protected abstract String sqlSelectField();

    protected abstract String selectFrom();

    protected abstract String selectOrderBy();

    protected abstract String selectGroupBy();

    protected abstract BaseDAO getBaseDAOCount();

    protected abstract void executeSelectPaginate(String sql) throws Exception;

    protected abstract List<SelectCondition> getSelectConditions();

    protected int calBeginIndex(int page, int pagesize) {
        int pageTemp = page;
        if (pageTemp <= 0)
            pageTemp = 1;
        int begin = (pageTemp - 1) * pagesize;
        return begin;
    }

    @Override
    protected String getSQL() {
        return null;
    }

    @Override
    protected void execute() throws Exception {
        String selectFrom = selectFrom();
        String sqlCondition = "";
        List<SelectCondition> selectConditions = getSelectConditions();
        selectConditions.sort((o1, o2) -> Boolean.compare(o1.isInGroup(), o2.isInGroup()));

        List<SelectCondition> selectConditionsNotInGroup = selectConditions.stream().filter(selectCondition -> !selectCondition.isInGroup()).collect(Collectors.toList());
        List<SelectCondition> selectConditionsInGroup = selectConditions.stream().filter(SelectCondition::isInGroup).collect(Collectors.toList());

        for (int i = 0; i < selectConditionsInGroup.size(); i++) {
            SelectCondition selectCondition = selectConditionsInGroup.get(i);
            if (i == 0) {
                if (selectFrom.endsWith("where")) {
                    sqlCondition += " (";
                    sqlCondition += selectCondition.firstSQL();
                    if (selectConditionsInGroup.size() == 1) {
                        sqlCondition += ")";
                    }
                } else {
                    sqlCondition += " (";
                    sqlCondition += selectCondition.toSQL();
                }
                continue;
            }
            if (i == selectConditionsInGroup.size() - 1) {
                sqlCondition += selectCondition.toSQL();
                sqlCondition += ")";
                continue;
            }
            sqlCondition += selectCondition.toSQL();
        }

        for (int i = 0; i < selectConditionsNotInGroup.size(); i++) {
            SelectCondition selectCondition = selectConditionsNotInGroup.get(i);
            if (i == 0) {
                if (selectFrom.trim().endsWith("where") && sqlCondition.isEmpty()) {
                    sqlCondition += selectCondition.firstSQL();
                } else {
                    sqlCondition += selectCondition.toSQL();
                }
                continue;
            }
            sqlCondition += selectCondition.toSQL();
        }

        if (selectConditions.size() == 0) {
            selectFrom = selectFrom.replace("where", "");
        }
        String sqlFromAndCondition = selectFrom + sqlCondition;

        String sqlCount = "select count(*) " + sqlFromAndCondition;
        totalRecord = getBaseDAOCount().getFirstCell(transid, sqlCount, Integer.class);
        String groupBy = selectGroupBy() == null ? "" : " group by " + selectGroupBy();
        String orderBy = selectOrderBy() == null ? "" : " order by " + selectOrderBy();
        String sqlSelect = sqlSelectField() + " " + sqlFromAndCondition + groupBy + orderBy + " limit " + beginIndex + "," + pageSize;
        executeSelectPaginate(sqlSelect);
    }
}
