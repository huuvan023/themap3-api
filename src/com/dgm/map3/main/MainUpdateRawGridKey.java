package com.dgm.map3.main;

import com.dgm.map3.core.manage.DBManager;
import com.dgm.map3.model.db.cmd.raw_data.DBGetCollectRawDataPageCmd;
import com.dgm.map3.model.db.cmd.raw_data.DBUpdateGridKeyCmd;
import com.dgm.map3.model.entity.raw_data.CollectData;
import com.dgm.map3.utils.OpenLocationHelper;
import com.ligerdev.appbase.utils.db.BaseDAO;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class MainUpdateRawGridKey {
    static Logger logger = Logger.getLogger("Sssd");
    private static AtomicInteger jobCount = new AtomicInteger(0);
    private static Object lock = new Object();


    public static void main(String[] args) throws Exception {
        OpenLocationHelper.generateMapLevel8Inside();

        DBManager.initBaseDAO();
        int page = 0;
        int pageSize = 3000;
        long lastId = 0;
        DBGetCollectRawDataPageCmd dbGetRawData = new DBGetCollectRawDataPageCmd("transid", "channel", page, pageSize, lastId);
        dbGetRawData.exec();
        List<CollectData> rawData = dbGetRawData.getRawDataList();

        int corePoolSize = 3;
        int maximumPoolSize = 5;
        int queueCapacity = 2000;
        LinkedBlockingQueue linkedBlockingQueue = new LinkedBlockingQueue(queueCapacity);
        ThreadPoolExecutor executorService = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 10, TimeUnit.SECONDS, linkedBlockingQueue);

        while (rawData != null && rawData.size() > 0) {
            logger.info("sync data : " + rawData.size());
            List<CollectData> finalSliData = rawData;
            jobCount.incrementAndGet();
            List<CollectData> finalRawData = rawData;
            executorService.execute(() -> {
                try {
                    logger.info(finalRawData.size() + "Size ne");
                    for (CollectData raw: finalRawData) {
                        String olc = OpenLocationHelper.getGridCodeFromLatLng(raw.getLat(),raw.getLng());
                        DBUpdateGridKeyCmd rq = new DBUpdateGridKeyCmd("", "", olc, raw.getId());
                        rq.exec();
                    }
                } catch (Exception e) {
                    logger.info(e.getMessage());
                } finally {
                    int count = jobCount.decrementAndGet();

                    if (count == 0) {
                        logger.info("finish sync all sli data");
                    }
                }
            });
            page++;
            lastId = rawData.get(rawData.size() - 1).getId();
            DBGetCollectRawDataPageCmd cmd = new DBGetCollectRawDataPageCmd("transid", "channel", page, pageSize,lastId);
            cmd.exec();
            rawData = cmd.getRawDataList();
            logger.info("collect data: " + rawData.size());
        }
        logger.info("outt =============");
    }
}
