package com.dgm.map3.main.data;

import lombok.Data;

import java.util.Date;

@Data
public class BaseResponse<T> {
    private String requestId;
    private Date startTime;
    private Date responseTime;
    private long duration;
    private ResultResponse result;

    private T data;

    public void setDuration(long start) {
        this.startTime = new Date(start);
        this.duration = System.currentTimeMillis() - start;
    }


    public BaseResponse(String requestId) {
        this.requestId = requestId;
        responseTime = new Date();
    }


}
