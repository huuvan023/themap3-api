package com.dgm.map3.main;

import com.dgm.map3.api.service.GeneralService;
import com.dgm.map3.core.config.Cfg;
import com.dgm.map3.core.manage.DBManager;
import com.dgm.map3.core.process.ThreadManager;
import com.ligerdev.appbase.utils.textbase.ConfigsReader;
import com.tbv.utils.app.AbsAppBase;
import com.tbv.utils.config.base.AbsConfigUtils;
import com.tdx.http.server.RestfulServer;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.apache.log4j.Logger.getLogger;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 01/02/20
 */
public class MainApplication extends AbsAppBase {
    static Logger logger = getLogger(MainApplication.class);
    //    public static DbModule dbModule;
    public static final ExecutorService service = Executors.newCachedThreadPool();
    protected static MainApplication app = null;
    public static RestfulServer restServer;


    public MainApplication(String pathConf) {
        super(pathConf);
    }

    public static synchronized MainApplication getInstance(String pathConf) {
        if (app == null) {
            app = new MainApplication(pathConf);
        }
        return app;
    }

    public static void main(String[] args) {
        try {
            String pathConf = args != null && args.length > 0 ? args[0] : "";
            app = getInstance(pathConf);
            app.init("log4j.xml");
            app.start();
            app.addShutdownHook();

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            try {
                app.stop();
            } catch (Exception e) {
                logger.warn(e, e);
                System.exit(9);
            }
        }
    }

    @Override
    public void initConfig() {
        DOMConfigurator.configureAndWatch("./config/log4j.xml");
        ConfigsReader.init(Cfg.class, "./config/config.xml");
//        disableLog();
    }


    @Override
    public void start() throws Exception {
//        ThreadManager.initThread();
        DBManager.initBaseDAO();
//        UserManager.syncUserFromDB();
//        DefineManager.syncServerResources();
//        DefineManager.syncDefineAddress();
        //==============================
        ThreadManager.initThreadSyncDB();

        restServer = new RestfulServer(GeneralService.class.getPackage().getName(), Cfg.API_PORT, Cfg.API_CONTEXT_PATH, "/*");
        restServer.publish();
        logger.info(String.format("Started HTTP Server with url: http://localhost:%d%s", Cfg.API_PORT, Cfg.API_CONTEXT_PATH));
        logger.info("-----------------------------MODULE IS STARTED !-----------------------------");
    }

    @Override
    public void stop() throws Exception {
        AbsConfigUtils.stopAllReloadThread();
        ThreadManager.stopThread();
//        if (threadSyncDB != null) {
//            threadSyncDB.doneThread();
//        }
        restServer.shutdown();
    }
}
