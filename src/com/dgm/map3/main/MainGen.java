package com.dgm.map3.main;

import com.ligerdev.appbase.utils.db.BaseDAO;
import com.ligerdev.appbase.utils.db.GenDbSource;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/02/21
 */
public class MainGen {
    public static void main(String[] args) throws Exception {
        String sql = "select * from collect.collect_data limit 1";
        String tableName= "collect_data";
        String keyName = "id";
        String packageName = "";
        org.apache.log4j.xml.DOMConfigurator.configureAndWatch("./config/log4j.xml");
        GenDbSource.genDTO(BaseDAO.POOL_NAME_MAIN, tableName, sql, keyName, packageName);
    }
}
