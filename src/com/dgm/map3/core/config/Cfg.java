package com.dgm.map3.core.config;

import com.ligerdev.appbase.utils.textbase.ConfigsReader;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 12/24/20
 */
public class Cfg extends ConfigsReader {
    public static boolean MONITOR_APPLICATION_ACTIVE = true;
    public static int API_PORT = 6996;
    public static String API_CONTEXT_PATH = "/api";

    public static String ELASTIC_SEARCH_HOST = "http://es.poicollector.com/";
    public static String ELASTIC_SEARCH_DGM_COLLECT_GRID_MAPS = "dgm_collect_grid_maps";

    //===============Incident===============
    public static String INCIDENT_UPLOAD_FILE_PATH = "./upload";
    public static String BASE_IMAGE_URL = "http://localhost";
    public static Integer INCIDENT_SERVER_RESOURCE_ID = 0;

    //===============Upload===============
    public static String FILE_ROOT_PATH = "/opt/";
    public static String FILE_PATH_UPLOAD_COLLECT_REPORT = "/opt/";
    public static String FILE_TEXT_PATH = "/opt/";

    public static boolean LOG_REQUEST_INBOUND_ENABLE = false;

    public static int MONITOR_DETECT_DISTRIBUTE_QUOTA = 1000;

    public static String CACHE_FILE_MAPS_PATH = "/opt/";

    @Override
    public void readPropeties() throws Exception {

        API_PORT = getInt("api|port");
        API_CONTEXT_PATH = getString("api|context_path");

        INCIDENT_SERVER_RESOURCE_ID = getInt("resource|server_resource_id");

        String incidentUploadPathKey = "upload|incident_upload_path|path";
        if (isExistAttr(incidentUploadPathKey)) {
            INCIDENT_UPLOAD_FILE_PATH = getStringAttr(incidentUploadPathKey);
        }
        String fileRootPath = "upload|file_root_path|path";
        if (isExistAttr(fileRootPath)) {
            FILE_ROOT_PATH = getStringAttr(fileRootPath);
        }
    }
}
