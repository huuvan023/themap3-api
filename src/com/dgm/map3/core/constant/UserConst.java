package com.dgm.map3.core.constant;


/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public interface UserConst {
    int USER_STATUS_ACTIVE = 1;
    int USER_STATUS_INACTIVE = 2;


    int INCIDENT_COLLECTOR= 1;
    int INCIDENT_INPUTTER = 2;
    int INCIDENT_SPECIAL_COLLECTOR = 3;
    int MODERATOR = 10;
    int ADMIN = 11;
}
