package com.dgm.map3.core.constant;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public interface UserActionConst {
    //Collector
    int COLLECTOR_SUBMIT = 1;

    //Inputter
    int INPUTTER_SUBMIT = 2;
    int INPUTTER_REJECT = 3;
}
