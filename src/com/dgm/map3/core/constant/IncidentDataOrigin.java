package com.dgm.map3.core.constant;

public interface IncidentDataOrigin {
    int FROM_VENDOR = 1;
    int FROM_APP = 2;

}
