package com.dgm.map3.core.constant;


/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/4/22
 */
public interface IncidentConst {
    // Incident Raw Status
    int INCIDENT_RAW_INITIAL = 0;
    int INCIDENT_RAW_ASSIGNED = 1;
    int INCIDENT_RAW_ACCEPTED = 2;
    int INCIDENT_RAW_REJECTED = 3;

    //Incident Inputted Data Status
    int INCIDENT_SUBMITTED = 5;


    //General
    int CLIENT_ACCEPTED = 6;
    int CLIENT_REJECTED = 7;

    //Expired timer
    int EXPIRED_TIME_TO_REVOKE = 5; //mins
}
