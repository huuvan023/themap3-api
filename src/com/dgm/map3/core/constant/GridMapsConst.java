package com.dgm.map3.core.constant;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 02/15/20
 */
public interface GridMapsConst {
    int INITIAL = 0;
    int COLLECTOR_ASSIGNED = 1;
    int COLLECTOR_DOING = 2;
    int COLLECTOR_SUBMIT_DONE = 3;
    int CHECKER_ASSIGNED = 4;
    int CHECKER_REJECT = 5;
    int CHECKER_ACCEPT = 6;
}
