package com.dgm.map3.core.process;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 05/20/21
 */
public class ThreadManager {
    private static ThreadSyncDB threadSyncDB;

    public static void initThreadSyncDB() {
        threadSyncDB = new ThreadSyncDB();
        threadSyncDB.setName("threadSyncDB");
        threadSyncDB.start();
    }

    public static void stopThread() {
        if (threadSyncDB != null) {
            threadSyncDB.finishThread();
        }
    }
}
