package com.dgm.map3.core.manage;

import com.ligerdev.appbase.utils.cache.CacheSyncFile;

import java.io.Serializable;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/11/22
 */
public class FileCacheSampleManager {
    static final String CACHE_KEY = "INCIDENT_RAW_DATA";
    static CacheSyncFile cacheSyncFile = CacheSyncFile.getInstance(CACHE_KEY, 10000);

    public static void putCache(String key, Object obj) {
        cacheSyncFile.put(key, (Serializable) obj, 600);
    }

    public static Object getData(String key) {
        return cacheSyncFile.getObjAndRefresh(key);
    }
}
