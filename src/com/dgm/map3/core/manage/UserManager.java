package com.dgm.map3.core.manage;

import com.dgm.map3.model.db.cmd.user.DBSyncUserCmd;
import com.dgm.map3.model.entity.user.UserEntity;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class UserManager {
    private static ConcurrentHashMap<Integer, UserEntity> mapUserID = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, UserEntity> mapUser = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, String> mapUsernameUUID = new ConcurrentHashMap<>();

    public static Object object = new Object();

    public static void syncUserFromDB() {
        DBSyncUserCmd dbSyncUserCmd = new DBSyncUserCmd("SYNC", "UserAndRoleManager");
        dbSyncUserCmd.exec();
        synchronized (object) {
            mapUser = dbSyncUserCmd.getMapUserEntityConcurrentHashMap();
            mapUsernameUUID = dbSyncUserCmd.getMapUserNameConcurrentHashMapUUID();
            mapUserID = dbSyncUserCmd.getMapUserIdConcurrentHashMap();
        }
    }
    public static Collection<UserEntity> getListUser() {
        synchronized (object) {
            return mapUser.values();
        }
    }

    public static void putUser(UserEntity userEntity) {
        if (userEntity == null) {
            return;
        }
        synchronized (object) {
            mapUser.put(userEntity.getUsername(), userEntity);
            mapUserID.put(userEntity.getId(), userEntity);
            mapUsernameUUID.put(userEntity.getUuid(), userEntity.getUsername());
        }
    }
    public static boolean existByUserName(String username) {
        return mapUser.containsKey(username);
    }
    public static UserEntity getUserEntityByUserName(String username) {
       if (!mapUser.containsKey(username)) {
            return null;
        }
        return mapUser.get(username);
    }
    public static UserEntity getUserEntityByID(int id) {
        if (!mapUserID.containsKey(id)) {
            return null;
        }
        return mapUserID.get(id);
    }
}
