package com.dgm.map3.core.manage;

import com.dgm.map3.model.db.cmd.define.DBSyncServerResourceCmd;
import com.dgm.map3.model.entity.define.ServerResourcesEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là huuvan, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 03/08/22
 */
public class DefineManager {
    static final String CACHE_NAME = "CACHE_DEFINE_CONFIG";
    private static ConcurrentHashMap<Integer, ServerResourcesEntity> serverResourcesConcurrentHashMap;

    public static void syncServerResources() {
        DBSyncServerResourceCmd command = new DBSyncServerResourceCmd("DB_SYNC_SERVER_RESOURCE", CACHE_NAME);
        command.exec();
        List<ServerResourcesEntity> serverResources = command.getData();
        serverResourcesConcurrentHashMap = new ConcurrentHashMap<>();
        for (ServerResourcesEntity serverResource: serverResources) {
            serverResourcesConcurrentHashMap.put(serverResource.getId(), serverResource);
        }
    }
    public static List<ServerResourcesEntity> getAllServerResource() {
        if(serverResourcesConcurrentHashMap.size() == 0) {
            return new ArrayList<>();
        }
        return new ArrayList<>(serverResourcesConcurrentHashMap.values());
    }
}
