package com.dgm.map3.core.manage;

import com.ligerdev.appbase.utils.db.BaseDAO;

/**
 * @title:
 * @author: nếu code trong class này chạy đúng thì tác giả là truong.do, còn nếu sai.. tác giả là ai, tôi không biết!!
 * @date: 05/06/20
 */
public class DBManager {
    public static BaseDAO baseDAO;

    public static void initBaseDAO() {
        baseDAO = BaseDAO.getInstance(BaseDAO.POOL_NAME_MAIN);
    }
}
